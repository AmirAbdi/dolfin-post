#
# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <njansson@csc.kth.se> wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return Niclas Jansson
# ----------------------------------------------------------------------------
#

AC_DEFUN([AX_SILO],[
	AC_ARG_WITH([silo],
	AS_HELP_STRING([--with-silo=DIR],
	[Directory for silo]),
	[	   
	if test -d "$withval"; then
		ac_silo_path="$withval";
		SILO_LDFLAGS="-L$ac_silo_path/lib"  
		SILO_CPPFLAGS="-I$ac_silo_path/include"
	fi
	],)

	AC_ARG_WITH([silo-libdir],
	AS_HELP_STRING([--with-silo-libdir=LIBDIR],
	[Directory for silo library]),
	[
	if test -d "$withval"; then
	   ac_silo_libdir="$withval"
	fi
	],)

	if test -d "$ac_silo_libdir"; then	   
	   SILO_LDFLAGS="-L$ac_silo_libdir"  	   	   
	fi

	if test -d "$ac_silo_path"; then
	   CPPFLAGS_SAVED="$CPPFLAGS"
	   LDFLAGS_SAVED="$LDFLAGS"
	   CPPFLAGS="$SILO_CPPFLAGS $CPPFLAGS"
	   LDFLAGS="$SILO_LDFLAGS $LDFLAGS"
	   export CPPFLAGS
	   export LDFLAGS
	fi

	AC_CHECK_HEADER([silo.h],[have_silo_h=yes],[have_silo_h=no])

	# Test if built with hdf5 (szip)
	AC_CHECK_LIB(siloh5, DBClose,
	[have_silo_hdf5=yes;SILO_LIBS="-lsiloh5 -lhdf5 -lsz -lz -lm -lstdc++"],
	[have_silo_hdf5=no],[-lsiloh5 -lhdf5 -lsz -lz -lm -lstdc++])

	if test x"${have_silo_hdf5}" = xno; then
		# Test if built with hdf5 (zlib)
 		AC_CHECK_LIB(siloh5, DBFileVersion,
		[have_silo_hdf5=yes;SILO_LIBS="-lsiloh5 -lhdf5 -lz -lm -lstdc++"],
		[have_silo_hdf5=no],[-lsiloh5 -lhdf5 -lz -lm -lstdc++])
	fi


	if test x"${have_silo_hdf5}" = xno; then
		# Fallback to legazy PDB driver
		AC_CHECK_LIB(silo, DBClose,[have_silo=yes;SILO_LIBS="-lsilo -lm"],
		[have_silo=no],[-lsilo -lm])
	else
		have_silo=yes
	fi

	AC_SUBST(SILO_LIBS)
	if test x"${have_silo}" = xyes; then
	   AC_DEFINE(HAVE_SILO,1,[Define if you have the Silo library.])
	   if test x"${have_silo_hdf5}" = xyes; then
	      AC_DEFINE(HAVE_SILO_HDF5,1,[Define if Silo is built with HDF5.])
	   fi
	else
		if test -d "$ac_silo_path"; then	
		   CPPFLAGS="$CPPFLAGS_SAVED"
		   LDFLAGS="$LDFLAGS_SAVED"
		fi
	fi
])


