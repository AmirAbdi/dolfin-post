/*
Copyright (c) 2011, Niclas Jansson <njansson@csc.kth.se>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of the author nor the names of its contributors may be
      used to endorse or promote products derived from this software without
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <stdlib.h>
#include <silo_export.h>
#include <dolfin_binary.h>
#include <dolfin_post.h>

#ifdef HAVE_SILO
void write_silomesh(DBfile *dbfile, Mesh *mesh, double *t) {

  int i, j, k, nshapetypes, nzones;
  int shapecounts[1], shapesize[1], shapetype[1];
  double *xyz[mesh->dim];
  double *xyz_coords[mesh->dim];
  int *silo_cells;

  DBoptlist *optlist;
  if (t) {
    optlist = DBMakeOptlist(1);
    DBAddOption(optlist, DBOPT_DTIME, t);
  }
  else
    optlist = NULL;

  shapecounts[0] = mesh->ncells;
  shapesize[0] = mesh->type;


  switch(mesh->type)
  {
  case 2:
    shapetype[0] = DB_ZONETYPE_BEAM;
    break;
  case 3:
    shapetype[0] = DB_ZONETYPE_TRIANGLE;
    break;
  case 4:
    shapetype[0] = DB_ZONETYPE_TET;
    break;
  default:
    /* Add error message */
    break;
  }

  silo_cells = malloc(mesh->ncells * mesh->type * sizeof(int));

  k = 0;
  for (i = 0; i < mesh->dim; i++)
    xyz[i] = malloc(mesh->nvertices * sizeof(double));

  k = 0;
  for (i = 0; i < mesh->nvertices * mesh->dim ; i += mesh->dim, k++)
    for (j = 0; j < mesh->dim; j++)
      xyz[j][k] = mesh->vertices[i + j];

  for (i = 0; i < mesh->dim; i++)
    xyz_coords[i] = &xyz[i][0];

  for (i = 0; i < mesh->ncells * mesh->type; i++)
    silo_cells[i] = mesh->cells[i] + 1;

#if SILO_VERS_MIN > 0x05
  DBPutZonelist2(dbfile, "zonelist", 1, mesh->dim, silo_cells,
		 mesh->ncells * mesh->type, 1, 0, 0,
		 shapetype, shapesize, shapecounts, 1, NULL);
#else
  DBPutZonelist(dbfile, "zonelist", 1, mesh->dim, silo_cells,
		mesh->ncells * mesh->type, 1, shapesize, shapecounts, 1);
#endif
  DBPutUcdmesh(dbfile, "mesh", mesh->dim , NULL, xyz_coords, mesh->nvertices, 1,
	       "zonelist", NULL, DB_DOUBLE, optlist);

  for (i = 0; i < mesh->dim; i++) {
    free(xyz[i]);
  }
  free(silo_cells);

  if (t)
    DBFreeOptlist(optlist);
}

void write_silopointdata(DBfile *dbfile, double *data, BinaryFunctionHeader hdr) {
  int i, j, k;
  char test[3] = {'x','y','z'};
  char *varnames[hdr.dim];
  double *xyz[hdr.dim];
  double *vars[hdr.dim];

  DBoptlist *optlist = DBMakeOptlist(1);
  DBAddOption(optlist, DBOPT_DTIME, &hdr.t);

  if (hdr.dim == 1) {
    DBPutUcdvar1(dbfile, hdr.name, "mesh", data, hdr.size, NULL, 0,
		 DB_DOUBLE, DB_NODECENT, optlist);
  }
  else {
    for (i = 0; i < hdr.dim; i++)
      xyz[i] = malloc(hdr.size/hdr.dim * sizeof(double));
    k = 0;
    for (i = 0; i < hdr.size ; i += hdr.dim, k++)
      for (j = 0; j < hdr.dim; j++)
	xyz[j][k] = data[i+j];


    for (i = 0; i < hdr.dim; i++)
      varnames[i] = &test[i];

    for (i = 0; i < hdr.dim; i++)
      vars[i] = &xyz[i][0];


    DBPutUcdvar(dbfile, hdr.name, "mesh", hdr.dim, varnames, vars,
		(hdr.size/hdr.dim), NULL, 0, DB_DOUBLE, DB_NODECENT, optlist);

    for (i = 0; i < hdr.dim; i++)
      free(xyz[i]);
  }
  DBFreeOptlist(optlist);
}
#else
void write_silomesh(void *dbfile, Mesh *mesh, double *t) {}

void write_silopointdata(void *dbfile, double *data, BinaryFunctionHeader hdr) {}
#endif
