/*
Copyright (c) 2011, Niclas Jansson <njansson@csc.kth.se>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the author nor the names of its contributors may be 
      used to endorse or promote products derived from this software without 
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#ifndef __VTK_EXPORT_H
#define __VTK_EXPORT_H

#include <stdio.h>

void write_encoded(FILE *fp, unsigned char *data, unsigned long usize);

void write_vtkheader_open(FILE *fp, int num_cells, int num_vertices);

void write_vtkheader_close(FILE *fp);

void write_vtkpoints(FILE *fp, double *data, int dim, int num_vertices);

void write_vtkcells(FILE *fp, int *cells, int type, int num_cells, int dim);

void write_vtkpointdata_open(FILE *fp);

void write_vtkpointdata_close(FILE *fp);

void write_vtkpointdata(FILE *fp, double *data, BinaryFunctionHeader hdr);

void write_vtkcelldata_open(FILE *fp);

void write_vtkcelldata_close(FILE *fp);

void write_vtkarray(FILE *fp, double *data, int size);

void write_pvdheader_open(FILE *fp);

void write_pvdheader_close(FILE *fp);

void write_pvddataset(FILE *fp, char *fname, double t);

#endif
