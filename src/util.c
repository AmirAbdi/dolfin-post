/*
Copyright (c) 2014, Niclas Jansson <leifniclas.jansson@riken.jp>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, 
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation 
      and/or other materials provided with the distribution.
    * Neither the name of the author nor the names of its contributors may be 
      used to endorse or promote products derived from this software without 
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

#include <util.h>

int bswap_int(int x) {
  union {
    int x;
    unsigned char b[4];
  } ein, eout;
  
  ein.x = x;
  eout.b[0] = ein.b[3];
  eout.b[1] = ein.b[2];
  eout.b[2] = ein.b[1];
  eout.b[3] = ein.b[0];
  return eout.x;  
}

double bswap_double(double x) {
  union {
    double x;
    unsigned char b[8];
  } ein, eout;
  
  ein.x = x;
  eout.b[0] = ein.b[7];
  eout.b[1] = ein.b[6];
  eout.b[2] = ein.b[5];
  eout.b[3] = ein.b[4];
  eout.b[4] = ein.b[3];
  eout.b[5] = ein.b[2];
  eout.b[6] = ein.b[1];
  eout.b[7] = ein.b[0];
  return eout.x;  
}

void bswap_hdr(BinaryFileHeader *mesh_hdr) {
  mesh_hdr->magic = bswap_int(mesh_hdr->magic);
  mesh_hdr->bendian = bswap_int(mesh_hdr->bendian);
  mesh_hdr->pe_size = bswap_int(mesh_hdr->pe_size);
  mesh_hdr->type = bswap_int(mesh_hdr->type);
}

void bswap_fhdr(BinaryFunctionHeader *f_hdr) {
  f_hdr->dim = bswap_int(f_hdr->dim);
  f_hdr->size = bswap_int(f_hdr->size);
  f_hdr->t = bswap_double(f_hdr->t);
}

void bswap_data(double *data, int n) { 
  int i;
  
  for (i = 0; i < n; i++) 
    data[i] = bswap_double(data[i]); 
}

void bswap_iarr(int *data, int n) { 
  int i;
  
  for (i = 0; i < n; i++)
    data[i] = bswap_int(data[i]); 
}
