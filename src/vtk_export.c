/*
Copyright (c) 2011, Niclas Jansson <njansson@csc.kth.se>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of the author nor the names of its contributors may be
      used to endorse or promote products derived from this software without
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#ifdef HAVE_GLIB
#include <glib.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <zlib.h>
#include <dolfin_binary.h>

#ifdef HAVE_GLIB
void write_encoded(FILE *fp, unsigned char *data, unsigned long usize) {
  unsigned long csize;
  unsigned char *cdata;
  gchar *b64_header, *encoded;
  uint32_t header[4];

  csize =  (usize + (((usize)/1000)+1)+12);
  cdata = malloc(csize * sizeof(unsigned char));
  compress((Bytef *) cdata, &csize, (const Bytef *) data, usize);
  encoded = g_base64_encode(cdata, (gsize) csize);
  free(cdata);	     

  header[0] = 1;
  header[1] = (uint32_t) usize;
  header[2] = 0;
  header[3] = (uint32_t) csize;
  b64_header = g_base64_encode((const unsigned char *) header, 
			       (gsize) 4 * sizeof(uint32_t));
  
  fprintf(fp, "%s", b64_header);
  fprintf(fp, "%s\n", encoded);

  g_free(b64_header);
  g_free(encoded);
}
#else
void write_encoded(FILE *fp, unsigned char *data, unsigned long usize) {}
#endif

void write_vtkheader_open(FILE *fp, int num_cells, int num_vertices) {
  int x = 1;
  
  if (*(char *)&x == 1)
    fprintf(fp, "<VTKFile type=\"UnstructuredGrid\" "
	    "version=\"0.1\" byte_order=\"LittleEndian\" "
	    "compressor=\"vtkZLibDataCompressor\">\n");
  else
    fprintf(fp, "<VTKFile type=\"UnstructuredGrid\" "
	    "version=\"0.1\" byte_order=\"BigEndian\" "
	    "compressor=\"vtkZLibDataCompressor\">\n");
  fprintf(fp, "<UnstructuredGrid>  \n");
  fprintf(fp, "<Piece  NumberOfPoints=\" %8u\"  NumberOfCells=\" %8u\">  \n",
          num_vertices, num_cells); 
}

void write_vtkheader_close(FILE *fp) {
  fprintf(fp, "</Piece> \n </UnstructuredGrid> \n </VTKFile>");  
}

void write_vtkpoints(FILE *fp, double *data, int dim, int num_vertices) {
  
  double *data_new;
  data_new = NULL;
  fprintf(fp, "<Points>  \n");
  fprintf(fp, "<DataArray  type=\"Float64\" "
	  "NumberOfComponents=\"3\"  format=\"binary\">  \n");
  if (dim == 2) {
    data_new = malloc(3 * num_vertices * sizeof(double));
    int i;
    for (i = 0; i < num_vertices; i++) {
      data_new[3*i] = data[2*i];
      data_new[3*i+1] = data[2*i+1];
      data_new[3*i+2] = 0.0;
    }
    write_encoded(fp, (unsigned char *) data_new, 
		  (dim+1) * num_vertices * sizeof(double));
  }
  else
    write_encoded(fp, (unsigned char *) data, 
		  dim * num_vertices * sizeof(double));
  fprintf(fp, "</DataArray>  \n");
  fprintf(fp, "</Points>  \n");
  if (data_new)
    free(data_new);
}

void write_vtkcells(FILE *fp, int *cells, int type, int num_cells, int dim) {
  int offsets;
  int *tmp, *tp;
  uint8_t *otmp, *op;

  fprintf(fp, "<Cells>  \n");
  fprintf(fp, "<DataArray  type=\"Int32\" "
	  "Name=\"connectivity\"  format=\"binary\">  \n");  
  write_encoded(fp, (unsigned char *) cells, 
		num_cells * type * sizeof(int));
  fprintf(fp, "</DataArray> \n");
  
  tmp = malloc(num_cells * sizeof(int));
  tp = &tmp[0];
  for (offsets = 1; offsets <= num_cells; offsets++)
    *(tp++) = offsets * type;

  fprintf(fp, "<DataArray  type=\"Int32\" "
	  "Name=\"offsets\" format=\"binary\">  \n");
  write_encoded(fp, (unsigned char *) tmp, num_cells * sizeof(int));
  fprintf(fp, "</DataArray> \n");
  free(tmp);
    
  otmp = malloc(num_cells * sizeof(uint8_t));
  op = &otmp[0];
  int cell_type;
  if (dim == 2)
    cell_type = 5;
  else if (dim == 3)
    cell_type = 10;
    
  for (offsets = 1; offsets <= num_cells; offsets++)
    *(op++) = cell_type;
  fprintf(fp, "<DataArray  type=\"UInt8\"  "
	  "Name=\"types\"  format=\"binary\">  \n");
  write_encoded(fp, (unsigned char *) otmp, num_cells * sizeof(uint8_t));
  fprintf(fp, "</DataArray> \n");
  fprintf(fp, "</Cells> \n");  
  free(otmp);

}

void write_vtkpointdata_open(FILE *fp) {
  fprintf(fp, "<PointData> \n");
}

void write_vtkpointdata_close(FILE *fp) {
  fprintf(fp, "</PointData> \n");
}

void write_vtkpointdata(FILE *fp, double *data, BinaryFunctionHeader hdr) {
  if (hdr.dim == 1) 
    fprintf(fp, "<DataArray  type=\"Float64\" "
	    "Name=\"%s\"  format=\"binary\">   \n", hdr.name); 
  else
    fprintf(fp, "<DataArray  type=\"Float64\" "
	    "Name=\"%s\"  NumberOfComponents=\"%d\" format=\"binary\">  \n",
	    hdr.name, hdr.dim);
  write_encoded(fp, (unsigned char *) data, hdr.size * sizeof(double));
  fprintf(fp, "</DataArray> \n");
}

void write_vtkcelldata_open(FILE *fp) {
  fprintf(fp, "<CellData Scalars=\"U\"> \n");
}

void write_vtkcelldata_close(FILE *fp) {
  fprintf(fp, "</CellData> \n");
}

void write_vtkarray(FILE *fp, double *data, int size) {
    fprintf(fp, "<DataArray type=\"Float64\" Name=\"U\" format=\"binary\"> \n");
    write_encoded(fp, (unsigned char *) data, size * sizeof(double));
    fprintf(fp, "</DataArray> \n");
}

void write_pvdheader_open(FILE *fp) {
  fprintf(fp,"<?xml version=\"1.0\"?> \n"); 
  fprintf(fp,"<VTKFile type=\"Collection\" version=\"0.1\" > \n");
  fprintf(fp,"<Collection> \n");
}

void write_pvdheader_close(FILE *fp) {
  fprintf(fp,"</Collection> \n");
  fprintf(fp,"</VTKFile>\n");
}

void write_pvddataset(FILE *fp, char *fname, double t) {
  fprintf(fp,"<DataSet timestep=\"%e\" part=\"0\" file=\"%s\"/>\n", t, fname);
}


